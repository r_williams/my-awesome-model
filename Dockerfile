FROM python:3.8-slim

RUN mkdir /model
COPY src/requirements.txt /model/requirements.txt

RUN pip install --upgrade pip
RUN pip install -r /model/requirements.txt
ADD ./src/sentiment_model.py /model/sentiment_model.py

ENTRYPOINT ["python", "/model/sentiment_model.py"]
